# Övning 2—`make_unique`
Denna andra uppgift är mycket kortare, och baserar sig på de kunskaper du fått
i den förra uppgiften. I standardbiblioteket finns en funktion 
`std::make_unique`, och vi ska nu göra en motsvarande funktion för vår egna 
implementation av `unique_ptr`. 

## Metod
Din implementation ska kunna användas för att kompilera och köra test-filen
`exercise2.cpp`. Implementera funktionen i filen `make_unique.hpp`. Läs först 
igenom beskrivningen nedan.

Du kan testa om din kod fungerar genom att kompilera exercise2-programmet,
och köra det.

När du är klar med uppgiften, fyll gärna i 
[detta feedback-forumulär](https://docs.google.com/forms/d/e/1FAIpQLSeZ4UgPHXAb14yEcLLf43UDY7C0hxZQ-VAMmI2O_SJGSRCsfQ/viewform?usp=pp_url&entry.146653887=Uppgift+2).

## Beskrivning

Först måste vi kolla på vad den gör:

```cpp
std::unique_ptr<int> ptr = std::make_unique<int>(5);
```

Ok, så den gör grovjobbet av att skriva ut det här?
```cpp
std::unique_ptr<int> ptr = std::unique_ptr<int>(new int(5));
```
Verkar ju inte väldigt uppenbart hur det *egentligen* hjälper...
Eller? I det undre exemplet är den största punkten att notera att man måste 
skriva `int` två gånger. Det är ju inget särskilt för `int`, men kanske något 
mer problematiskt för `FragmentShaderAsset` eller `GUIApplicationServiceFactory`.
Så vi skippar alltså att skriva ut namnet två gånger. Woohoo. Men den fyller 
också en annan viktig funktion som inte är lika "självklar": man slipper skriva
`new` överhuvudtaget. I modern C++ (C++11 och senare) försöker man i högsta grad 
att inte skriva ut `new` i koden, då varje `new` normalt sett måste matchas med en
`delete`. Det är ett problem vi löste genom att byta till smart pointers till 
att börja med, och nu ser vår kod kanske till och med konstig ut, med `new`
på en massa ställen, men inga `delete`. Att byta förstärker alltså också
programmerarens rekommenderade tankesätt: skriv inte `new`.

Det är vad `make_unique` gör för hur vi läser koden, men vad gör den egentligen?
Om vi kollar på ett annat exempel:
```cpp
class Foo {
public:
    Foo(int x) : x(x) {}
private:
    int x;
};
// "traditionellt"
auto ptr = unique_ptr<Foo>(new Foo(42));
// make_unique
auto ptr2 = make_unique<Foo>(42);
```

På den näst sista raden konstruerar vi själva en `Foo` med 5 som argument. 
Som du kanske gissat gör den sista raden samma sak, men `make_unique` 
konstruerar objektet åt oss. Hur då? ...Antagligen på samma sätt som vi gör.
Men funktionen måste ju kunna konstruera vad som helst, med vilka argument som 
helst. Detta leder oss onekligen in i templates. Men vi skjuter lite 
grann på dem för att också nämna en annan poäng: vad händer om du t.ex. vill 
Move-constructa objektet? Mer om det senare, template-dags!

### Variadric templates
Vi vet än så länge två saker: vi ska konstruera ett nytt objekt av vilken typ 
som helst, och vi ska kunna ta in, och vidarebefordra, en valfri mängd argument.
Låter som att det blir en template-funktion då...

```cpp
template<typename T>
unique_ptr<T> make_unique(/* ??? */) {
}
```

Men hur gör vi nu då? Vi kan ju inte bara ta hur många argument som helst, eller
vilka som helst, för den delen. Normalt sett. Men vi måste göra det, och hör och
häpna, det finns ett sätt! Se titeln på denna sektion för namnet.

Vad detta innebär illustreras kanske enklast med ett exempel:
```cpp
template<typename... Args> // 1
double sum(Args... nums) { // 2
    return (0 + ... + nums); // 3
}
```
Nu är det mycket på en gång, så vi bryter ner det bit för bit.

1. Notera här `...` efter `typename`. Det betyder tillsammans med resten 
   att "`Args` är en *template parameter pack* [`...`] av typer [`typename`]". 
   Det parameter packs låter oss göra är 
   deklarera att vi kan ta ett obegränsat antal template-parametrar, noll eller 
   fler. Vi döper inte dessa parametrar, utan alla trycks ihop i ett enda paket 
   `Args`. Endast det sista template-argumentet kan vara en parameter pack, så 
   `template<int NumStuff, typename... stuffs>` är OK, men däremot inte
   `template<typename stuffs..., typename LastElement>`. Både klass- och 
   funktions-templates kan vara variadriska.
2. Här deklarerar vi resten av funktionssignaturen. Som argument tar vi alltså 
   ett obegränsat antal argument av typen Args som argumentet `nums`, och 
   argumenten vi får in fylls bekvämt automatiskt in i `Args`-parametern tack 
   vare argument-deduction.
   Exempel:
   ```cpp
   // Om vi skriver
   double x = sum(5, 4, 3);
   // Så instanseras nu templaten som
   template<>
   double sum<int, int, int>(int nums__1, int nums__2, int nums__3);
   
   double y = sum(3.14, 42);
   template<>
   double sum<double, int>(double num__1, int num__2)
   ```
   Så template parameter packs låter oss alltså ta en obegränsad mängd argument 
   av valfria typer. Hur... *använder* vi dem? Se nästa punkt.
3. Här är själva funktionskroppen. Vi returnerar alltså summan av... nånting.
   Det som händer här är att vi "packar upp", expanderar, parameter packen i 
   dess komponenter. I just det här fallet är detta dessutom en *fold expression*, 
   vilket är när detta görs kring operatorer. Koden expanderar då t. ex. till detta:
   ```cpp
   template<>
   double sum<int, int, int>(int __nums_1, int __nums_2, int __nums_3) {
       return 0 + __nums_1 + __nums_2 + __nums_3;
   }
   ```
   Parameter packs kan packas upp i väldigt många tänkbara situationer: 
   i funktionsparametrar `func(1, args...)`, *constructors* 
   `T t("str", args...)`, *initializer lists* `{5.05, "float", args...}`, 
   fold expressions som ovan, etc.
   
### Perfect forwarding
I denna sektion diskuterar *value categories*. De är väldigt relevanta till
situationen, eftersom `make_unique` måste bibehålla dem korrekt för argumenten 
som vidarebefordas till constructorn. I grund och botten finns det två stora
kategorier: *lvalues*, och *rvalues*. Sedan C++17 är detta *glvalues*, 
*xvalues*, *lvalues*, *rvalues*, och *prvalues*, men det är inte relevant för 
oss just nu. Om du är intresserad kan du läsa mer om det 
[här](https://medium.com/@barryrevzin/value-categories-in-c-17-f56ae54bccbe) 
och [här](https://en.cppreference.com/w/cpp/language/value_category).

Den viktigaste poängen att uttrycka här är att value categories inte handlar om 
typer, eller om någon slags "objekt-kategori", eller "variabel-kategori". De 
handlar om *uttryck*. Den snabba förklaringen är att det handlar om en 
svaret på en enda fråga: 
Kan jag säkert move:a från saken i uttrycket?

#### lvalues
En *lvalue* är ett uttryck som *namnger* ett objekt, en funktion 
eller en bitfield, såhär:
```cpp
void foo();
void bar(std::function<void()>);
int x = 5;

int y = x; // "x" är en lvalue.
bar(foo); // "foo" är en lvalue.

void foo(int&& x) {
    int y = x; // "x" är en ***lvalue***.
}
```
Notera särskilt här att även när man tar en rvalue-reference som argument är 
det fortfarande en lvalue att referera till den: det är *uttrycket*, inte typen,
och eftersom den har ett namn i uttrycket så är det en lvalue.

Snabbregeln för att kolla om något är en lvalue är "står det ett namn?". Om det
bara står ett namn är det antagligen en lvalue.

#### rvalues
En *rvalue* är ett uttryck som saknar namn (prvalue, "pure rvalue"), eller ett 
uttryck som har ett namn, men kommer sluta finnas (xvalue, "e**x**piring"). 
rvalues är kortfattat saker som kan move:as ifrån utan problem. Exempel:
```cpp
int x = 5; // "5" är en prvalue.
unique_ptr<int> ptr(new int);
auto ptr2 = std::move(ptr); // "std::move(ptr)" är en xvalue.

unique_ptr<int> get_pointer() { return unique_ptr<int>(new int (5)); }
ptr2 = get_pointer(); // "get_pointer()" är en prvalue.

struct Foo { int x; };
int y = Foo{5}.x; // "Foo{5}.x" är en xvalue.
```

#### "Forwarding"
Så vad är allt det här bra för att veta då? Problemet är att om vi försöker 
implementera vår `make_unique` utan att ta value categories i åtanke kommer inte
constructors för objekt att anropas med rätt argument. Som ovan nämndes att 
saker som "har namn" är lvalues. Alla funktionsargument har namn, även om det är
en variadrisk template-funktion. Du kanske börjar ana problemet redan nu, men vi
demonstrerar med ett exempel.
```cpp
struct Foo {
    Foo(unique_ptr<int> ptr) : ptr(std::move(ptr)) {}
    unique_ptr<int> ptr;
};
template<typename T, typename Arg>
unique_ptr<T> make(Arg&& arg) {
    // Vi skriver && på arg, för att kunna flytta saker om det går. 
    // Om det inte går binds de fortfarande som lvalues...
    return unique_ptr<T>(new T(arg));
}
auto ptr = make<int>(1); // allt är frid och fröjd, int går att kopiera.
// Kompilationsfel: ptr går inte att kopiera.
auto foo = make<Foo>(std::move(ptr));
```
Ack och ve: trots att vi skriver `Arg&&` i argumentlistan så blir inte 
argumenten till rvalues, så vi kan inte move:a från dem. Men vi kan ju förvandla
lvalues till rvalues med `std::move`...
```cpp
template<typename T, typename Arg>
unique_ptr<T> make(Arg&& arg) {
    return unique_ptr<T>(new T(std::move(arg)));
}
```
Med detta kommer våran ovan kod fungera... Men det är inte rätt. Nu beter sig 
koden som om allt vi stoppar in är rvalues, inte lvalues. Om vi stoppar in en 
lvalue, och då antar att vi kommer kopiera objektet, visar det sig att objektet
blev move:at ifrån istället. Allt blir till rvalues. Det är ju inte heller rätt.

Vi behöver alltså på något sätt skilja på om funktionen anropas med ett 
lvalue-argument eller ett rvalue-argument, och bara `std::move`:a om så är 
fallet. Det låter väldigt jobbigt. Men som tur är finns det (såklart) en 
funktion för detta i standardbiblioteket: `std::forward<T>()`! Den gör allt det
jobbiga åt en, så allt man behöver göra är att ta sitt argument som en template,
skriva `Arg&&` som typ man tar in, och använda `std::forward`. Detta leder till
att man använder en så kallad *forwarding reference*. Man "vidarebefodrar" helt
enkelt argumentet man tog in precis som det var: fick man en rvalue är det en 
rvalue, fick man en lvalue är det en lvalue. Med detta blir vår kod istället 
såhär:
```cpp
template<typename T, typename Arg>
unique_ptr<T> make(Arg&& arg) {
    // Notera: man MÅSTE skriva med <Arg>-delen på forward().
    // Får du konstiga fel glömde du säkert det.
    return unique_ptr<T>(new T(std::forward<Arg>(arg)));
}
```
Detta kallas för *perfect forwarding*. Med detta har du alla verktyg du behöver
för att implementera en `make_unique`.