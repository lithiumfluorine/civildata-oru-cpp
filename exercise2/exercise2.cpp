#include <fmt/format.h>
// clang-format off
#ifdef USE_SOLUTION
#   include "make_unique_solved.hpp"
#else 
#   include "make_unique.hpp"
#endif
// clang-format on

struct Foo {
    explicit Foo(std::shared_ptr<bool> ptr) : ptr(std::move(ptr)) {}
    std::shared_ptr<bool> ptr;
};

bool move_test() {
    auto ptr = std::make_shared<bool>();
    auto foo = orucpp::make_unique<Foo>(std::move(ptr));
    return foo->ptr.use_count() == 1 && !ptr;
}

bool copy_test() {
    auto ptr = std::make_shared<bool>();
    auto foo = orucpp::make_unique<Foo>(ptr);
    return ptr.use_count() == 2;
}

int main() {
    fmt::print(" === Exercise 2 test programme ===\n");
    fmt::print("Running tests.\n");
    bool move = move_test(); // test for proper move-construction.
    bool copy = copy_test(); // test for proper copy-construction.

    auto result = [](bool r) { return r ? ":)" : ":("; };
    fmt::print(" === RESULTS ===\n");
    fmt::print("Move: {}\n", result(move));
    fmt::print("Copy: {}\n", result(copy));
}
