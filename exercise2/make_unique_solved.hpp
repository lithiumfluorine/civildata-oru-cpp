#pragma once
#include <unique_ptr_solved.hpp>

namespace orucpp {

template<typename T, typename... Args>
unique_ptr<T> make_unique(Args&&... args) {
    return unique_ptr<T>(new T(std::forward<Args>(args)...));
}

}; // namespace orucpp
