#pragma once
#include <algorithm>
#include <type_traits>
#include <vector>

namespace util {

template<typename T, typename Alloc, typename Predicate>
std::vector<T, Alloc>::iterator erase_if(std::vector<T, Alloc>& vec, Predicate&& pred) {
    return vec.erase(std::remove(vec.begin(), vec.end(), std::forward<Predicate>(pred));
}

} // namespace util