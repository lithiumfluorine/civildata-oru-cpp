# Övningar i C++
Detta är en samling övningar i C++.

För att kolla på varje övning, klicka på den mappen i filträdet. När du gjort
en övning, fyll gärna i feedback-formuläret 
[här](https://docs.google.com/forms/d/e/1FAIpQLSeZ4UgPHXAb14yEcLLf43UDY7C0hxZQ-VAMmI2O_SJGSRCsfQ/viewform?usp=sf_link).

Notera att koden här är C++17.

## Översikt
 - Övning 1: Smart pointers—`unique_ptr`
 - Övning 2: Variadric templates, Perfect Forwarding—`make_unique`

# Kompilera
För att kompilera lösningarna och exempelkoden, gör såhär:
1. Klona repon: `git clone https://gitlab.com/lithiumfluorine/civildata-oru-cpp`.
2. Skapa build-mapp: `mkdir build && cd build`
3. Kör CMake: `cmake ..`
4. Kompilera: `cmake --build .`
