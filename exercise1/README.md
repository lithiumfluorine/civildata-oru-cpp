# Övning 1—unique_ptr
Denna första övning går ut på att implementera en simplifierad version av 
`std::unique_ptr`.

## Metod
Din implementation ska kunna användas för att kompilera och köra test-filen 
`exercise1.cpp`. Implementera klassen i filen `unique_ptr.hpp`. Läs igenom 
krav-listan nedan, och läs sedan Tips & tricks.

Du kan testa om din kod fungerar genom att kompilera exercise1-programmet, 
och köra det.

När du är klar med uppgiften, fyll gärna i 
[detta feedback-formulär](https://docs.google.com/forms/d/e/1FAIpQLSeZ4UgPHXAb14yEcLLf43UDY7C0hxZQ-VAMmI2O_SJGSRCsfQ/viewform?usp=pp_url&entry.146653887=Uppgift+1).

## Krav
C++-standarden ställer ett antal krav på `unique_ptr`:s implementation, varav 
några ignoreras pga komplexitet. Ett av de grundläggande kraven är att 
`unique_ptr` måste vara 
*[MoveConstructible](https://en.cppreference.com/w/cpp/named_req/MoveConstructible)*
och *[MoveAssignable](https://en.cppreference.com/w/cpp/named_req/MoveAssignable)*.
`unique_ptr` får dessutom **inte** uppfylla kraven 
*[CopyConstructible](https://en.cppreference.com/w/cpp/named_req/CopyConstructible)* eller 
*[CopyAssignable](https://en.cppreference.com/w/cpp/named_req/MoveAssignable)*.
Precis som `std::unique_ptr` ska pekaren som lagras inuti vår egna `unique_ptr`
raderas då instansen av klassen går out-of-scope: 

```cpp
void foo() {
    {
        unique_ptr<int> ptr(new int(-1234));
    } // ptr har nu raderats/delete:ats. 
}
```

Dessutom ska `unique_ptr`:n uppfylla kraven för att vara en *smart* pointer: 
den ska bete sig som om den vore en vanlig pekare:

```cpp
struct Foo {
    int bar;
};
unique_ptr<Foo> ptr(new Foo({7}));
ptr->bar = 42;
*ptr = Foo{777};
if (ptr) {
    ptr->bar += 111;
}
```

Utöver detta ska `unique_ptr` ha ett antal publika medlemsfunktioner:

### `T* get()`
Hämtar den lagrade pekaren.
```cpp
int* i = new int(42);
unique_ptr<int> ptr(i);
assert(i == ptr.get());
```

### `void reset()`
Raderar innehållet i pekaren så att den blir tom igen.
```cpp
struct Noise {
    ~Noise() {
        fmt::print("AAAAAAAA");
    }
};
unique_ptr<Noise> ptr(new Noise);
ptr.reset(); // AAAAAAAA
```

### `T* release()` 
Släpper lös och returnerar den lagrade pekaren utan att radera den.
```cpp
int* my_precious = new int(0);
{
    unique_ptr<int> ptr(my_precious);
    *ptr = 5;
    ptr.release();
}
assert(my_precious == 5);
delete my_precious;
```

# Tips & tricks
Vad är en smart pointer i grunden egentligen?
När man snackar om smart pointers brukar man ofta syfta på två olika saker 
samtidigt: automatiskt ägarskap av en resurs, **och** sättet man interagerar 
med typen i sin kod.

```cpp
// ptr kommer här raderas automatiskt när den går out-of-scope.
std::unique_ptr<int> ptr(new int(5));
// vi kan läsa och skriva till ptr precis som om det vore en vanlig int*.
*ptr = 4;
void foo(int x);
foo(*ptr);
```

## Ägarskap
`unique_ptr` är ett av de allra tydligaste exemplen på RAII 
(Resource Acquisition Is Initialization) i C++: när du instanserar klassen tar 
den ägarskapet av en pekare, och när klassinstansen förstörs raderas pekaren.
I grund och botten görs detta såhär:
```cpp
struct RAII {
    RAII(int* ptr) : data(ptr) {}
    ~RAII() noexcept {
        if (data) delete data;
    }
private:
    int* data{nullptr};
};
```

Men för att få det att faktiskt fungera måste man också—i enlighet med Rule of 
Five—korrekt definera Move-constructor, Move-assignment, Copy-constructor, och 
Copy-assignment.

## Operatorer
För att åstadkomma pekar-beteendet används *operator overloading*, mer specifikt 
av `operator->`, och `operator*`.

### `operator->`
Denna operator ska om den overloadas returnera en pekare till saken som 
"faktiskt ska kollas på". 

### `operator*`
Denna operator ska om den overloadas returnera en referens till saken som 
"fakiskt ska kollas på".

### `operator bool`, `operator!`
Detta låter kanske lite konstigt, men man kan ju kolla om en pekare är `nullptr`
genom att skriva
```cpp
int* ptr = // blah blah
if (ptr) {
    // do stuff
} else if (!ptr) { // onödigt, ja, men illustrativt.
    // do stuff2
}
```
så man borde kunna göra samma sak med en smart pointer.
