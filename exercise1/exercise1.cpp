#ifndef USE_SOLUTION
#include "unique_ptr.hpp"
#else
#include "unique_ptr_solved.hpp"
#endif

#include <fmt/format.h>
#include <memory>
#include <type_traits>
using namespace orucpp;

static_assert(!std::is_copy_constructible_v<unique_ptr<int>>, "unique_ptr must not be copy constructible!");
static_assert(!std::is_copy_assignable_v<unique_ptr<int>>, "unique_ptr must not be copy assignable!");

bool basic_test() {
    struct Foo {
        int bar{0};
    };
    unique_ptr<Foo> ptr(new Foo);
    return ptr->bar == 0 && (*ptr).bar == 0;
}

bool dtor_test() {
    auto shared_ptr = std::make_shared<int>(0);
    { unique_ptr<std::shared_ptr<int>> ptr(new std::shared_ptr<int>(shared_ptr)); }
    return shared_ptr.use_count() == 1;
}

bool move_test() {
    bool            move = false;
    int*            i    = new int(2);
    unique_ptr<int> ptr(i);
    unique_ptr<int> ptr2(std::move(ptr));
    *ptr2 = 3;
    // clang-format off
    if (   ptr2.get() != ptr.get() 
        && ptr.get()  != i 
        && ptr2.get() == i) move = true;
    // clang-format on
    ptr = std::move(ptr2);
    return move && ptr.get() == i && ptr2.get() != i;
}

int reset_test() {
    auto                             shared = std::make_shared<int>(0);
    unique_ptr<std::shared_ptr<int>> ptr(new std::shared_ptr<int>(shared));
    ptr.reset();
    // fmt::print("The number is 42, right? {}\n", bar);
    return shared.use_count() == 1;
}

bool bool_default_test() {
    unique_ptr<int> ptr(new int(1));
    unique_ptr<int> ptr2;
    // fmt::print("You see here a smiley face: ");
    return ptr && !ptr2;
}

bool release_test() {
    auto shared  = std::make_shared<int>(0);
    bool success = false;
    {
        unique_ptr<std::shared_ptr<int>> ptr(new std::shared_ptr<int>(shared));
        success   = shared.use_count() == 2;
        auto ptr_ = ptr.release();
        success   = success && !ptr;
        delete ptr_;
    }
    return success && shared.use_count() == 1;
}

int main() {
    fmt::print(" === Exercise 1 test programme ===\n");
    fmt::print("Running tests.\n");

    // Basic test of smartness
    bool basic = basic_test();
    // Test to make sure destructors are being called.
    bool dtor = dtor_test();
    // Test move ctor and move assignment
    bool move = move_test();
    // Test reset
    bool reset = reset_test();
    // Test default ctor and bool
    bool bool_default = bool_default_test();
    // Test release
    bool release = release_test();

    auto result = [](bool r) { return r ? ":)" : ":("; };
    fmt::print(" === RESULTS ===\n");
    fmt::print("Basic test:        {}\n", result(basic));
    fmt::print("Destructor:        {}\n", result(dtor));
    fmt::print("bool+default ctor: {}\n", result(bool_default));
    fmt::print("Move ctor+assign:  {}\n", result(move));
    fmt::print("reset():           {}\n", result(reset));
    fmt::print("release():         {}\n", result(release));
}
