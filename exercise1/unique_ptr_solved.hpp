#pragma once
#include <fmt/format.h>

namespace orucpp {

// Ändra detta till true för att printa när saker händer.
constexpr bool noisy = false;

template<typename T>
class unique_ptr {
public:
    unique_ptr() : data(nullptr) {
        if constexpr (noisy) {
            fmt::print("PTR: Default constructed!\n");
        }
    }
    unique_ptr(T* ptr) : data(ptr) {
        if constexpr (noisy) {
            fmt::print("PTR: Constructed with data! [{}]\n", (void*)ptr);
        }
    }
    unique_ptr(std::nullptr_t) : data(nullptr) {
        if constexpr (noisy) {
            fmt::print("PTR: Constructed with nullptr!\n");
        }
    }

    // Radera copy-constructor och copy-assignment.
    unique_ptr(const unique_ptr&) = delete;
    unique_ptr& operator=(const unique_ptr&) = delete;

    // Move constructor och assignment.
    unique_ptr(unique_ptr&& other) noexcept : data(nullptr) {
        move_from(other);
        if constexpr (noisy) {
            fmt::print("PTR: Move constructed!\n");
        }
    }
    unique_ptr& operator=(unique_ptr&& other) noexcept {
        move_from(other);
        if constexpr (noisy) {
            fmt::print("PTR: Move assigned!\n");
        }
        return *this;
    }

    ~unique_ptr() noexcept {
        if constexpr (noisy) {
            fmt::print("PTR: Deleting pointer! {}\n", (void*)data);
        }
        reset();
    }

    T* get() const noexcept {
        return data;
    }

    // Raderar innehållet i pekaren, så att den blir tom.
    void reset() noexcept(noexcept(delete data)) {
        if (data != nullptr) {
            if constexpr (noisy) {
                fmt::print("PTR: Resetting the pointer thingy!");
            }
            delete data;
            data = nullptr;
        }
    }
    // Släpper ägarskapet av den ägda pekaren.
    T* release() noexcept {
        T* d = data;
        data = nullptr;
        return d;
    }

    operator bool() const noexcept {
        return data;
    }
    bool operator!() const noexcept {
        return !operator bool();
    }
    // Delen som gör det till en "smart" pekare.
    T* operator->() const noexcept {
        return data;
    }
    T& operator*() const noexcept {
        return *data;
    }

private:
    void move_from(unique_ptr& other) noexcept {
        // Detta förhindrar att problem uppstår om man assignar till
        // samma objekt.
        std::swap(data, other.data);
    }
    T* data;
};

} // namespace orucpp
