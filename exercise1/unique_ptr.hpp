#pragma once
namespace orucpp {

template<typename T>
class unique_ptr {
public:
    unique_ptr(T* t) : data(t) {
    }

    // Add your stuff...

private:
    // [UR STUFF HERE]
    T* data;
};

} // namespace orucpp
