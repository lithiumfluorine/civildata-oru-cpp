cmake_minimum_required(VERSION 3.9)
set (CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED true)
project(civildata-cpp)

add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/third_party/fmt-5.3.0")
add_library(Utils INTERFACE)
target_include_directories(Utils INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/utils>
)
target_link_libraries(Utils INTERFACE
    fmt::fmt
)

add_subdirectory(exercise1)
add_subdirectory(exercise2)
